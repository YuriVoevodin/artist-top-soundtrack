#!/usr/bin/env python3

import re
import urllib.request

# TODO Use unicode
# TODO search complex artist names

def getArtistURL(artist):
    # TODO check using Space symbol in artist request
    searchURL = f'https://music.yandex.ru/search?text={artist}&type=artists'
    r = urllib.request.urlopen(searchURL).read()
    r = str(r)
    r = re.search(r'href=\"/artist/\d+\"', r)
    artist_id = re.search(r'/artist/\d+', r[0])
    return f'https://music.yandex.ru{artist_id[0]}/tracks'


def getSoundList(artistURL):
    r = urllib.request.urlopen(artistURL).read()
    r = str(r)
    trackList = re.findall(
        r'<a href=\"/album/\d+/track/\d+\" class=\"d-track__title deco-link deco-link_stronger\">(.+?)</a>', r)
    return trackList


artist = input()
artistURL = getArtistURL(artist)
track_list = getSoundList(artistURL)

for i in track_list:
    print(i)
print(len(track_list))
